USE music_db2;

-- a.) Find all artist with the letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";

-- b.) Find all songs that has a length less than 3:50
SELECT * FROM songs WHERE length < 350;

-- c.) Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
SELECT DISTINCT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON songs.album_id = albums.id;

-- d.) Join the 'artists' and 'albums' tables. (find all albums with the letter a in its name)
SELECT * FROM albums 
	JOIN artists ON albums.artist_id = artists.id
	WHERE album_title LIKE "%a%";

-- e.) Sort the albums in Z-A order. (show only the first 4 records)
SELECT * FROM albums ORDER BY album_title DESC
	LIMIT 4;	

-- f.) Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * FROM albums 
	JOIN songs ON songs.album_id = albums.id
	ORDER BY album_title DESC;



