USE music_db;

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 4);


INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 101);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 101);



-- MariaDB [music_db]> select * from artists;
-- +----+-------------------+
-- | id | name              |
-- +----+-------------------+
-- |  1 | Rivermaya         |
-- |  2 | Psy               |
-- |  3 | Weird AL Yankovic |
-- |  4 | Taylor Swift      |
-- |  5 | Lady Gaga         |
-- |  6 | Justin Bieber     |
-- |  7 | Ariana Grande     |
-- |  8 | Bruno Mars        |
-- +----+-------------------+
-- 8 rows in set (0.000 sec)

-- MariaDB [music_db]> select * from songs;
-- +----+----------------+----------+------------------------------------+----------+
-- | id | song_name      | length   | genre                              | album_id |
-- +----+----------------+----------+------------------------------------+----------+
-- |  1 | Gangnum Style  | 00:02:53 | K-POP                              |      100 |
-- |  2 | Kisapmata      | 00:02:59 | OPM                                |        2 |
-- |  6 | Fearless       | 00:02:46 | Pop rock                           |      101 |
-- |  7 | Love Story     | 00:02:13 | Country pop                        |      101 |
-- |  8 | State of Grace | 00:02:53 | Rock, alternative rock, arena rock |      102 |
-- |  9 | Red            | 00:02:04 | Country                            |      102 |
-- | 10 | Black Eyes     | 00:01:51 | Rock and roll                      |      103 |
-- | 11 | Shallow        | 00:02:01 | Country, rock, folk rock           |      103 |
-- +----+----------------+----------+------------------------------------+----------+
-- 8 rows in set (0.000 sec)

-- MariaDB [music_db]> select * from albums;
-- +-----+----------------+---------------+-----------+
-- | id  | album_title    | date_released | artist_id |
-- +-----+----------------+---------------+-----------+
-- |   2 | Trip           | 1996-01-01    |         1 |
-- | 100 | PSY 6          | 2012-01-01    |         2 |
-- | 101 | Fearless       | 2008-01-01    |         4 |
-- | 102 | Red            | 2012-01-01    |         4 |
-- | 103 | A Star Is Born | 2018-01-01    |         5 |
-- | 104 | Born This Way  | 2011-01-01    |         5 |
-- +-----+----------------+---------------+-----------+
-- 6 rows in set (0.000 sec)

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 253, "Rock, alternative rock, arena rock", 102);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 102);


INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-1-1", 5);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 151, "Rock and roll", 103);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 103);


-- [SECTION] ADVANCE SELECT

SELECT * FROM songs WHERE id != 11;

SELECT song_name, genre FROM songs WHERE id != 11;


-- Greater than or equal operator
SELECT * FROM songs WHERE id >= 11;

-- Less than or equal operator
SELECT * FROM songs WHERE id <= 11;

-- id >-5 and id<=11
SELECT * FROM songs WHERE id >=5 AND id <=11;

-- get something with a specific id
SELECT * FROM songs WHERE id = 11;


-- OR operator
SELECT * FROM songs WHERE id =1 OR id =5;

-- IN Operator
SELECT * FROM songs WHERE id IN (1, 11, 5);

-- PARTIAL OPERAOR
SELECT * FROM songs WHERE song_name LIKE "%e";
SELECT * FROM songs WHERE song_name LIKE "%a";

SELECT * FROM songs WHERE song_name LIKE "%e%";

SELECT * FROM songs WHERE album_id LIKE "10_";

--SORT record 
--Ascending
SELECT * FROM songs ORDER BY song_name ASC;

--Descending
SELECT * FROM songs ORDER BY song_name DESC;

--DISTINCT Records
SELECT DISTINCT genre FROM songs;


-- [section] Table joins:
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT DISTINCT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;


SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;



SELECT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;




SELECT DISTINCT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;















